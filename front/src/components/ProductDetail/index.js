import React from "react";
import PropTypes from "prop-types";

import { Container, Img, List, ListTitle, ListItem, ListLabel } from "./style";

const ProductDetail = ({ product, checkout, couponId, selectCoupon }) => (
  <Container>
    <Img src={product.image} alt={product.title} width="125" />

    <List>
      <ListTitle>Cupons</ListTitle>
      <ListItem>
        <ListLabel couponId={couponId}>
          <input
            onChange={e => selectCoupon(e.target.value)}
            type="radio"
            name="coupon"
            value=""
            checked={couponId === ""}
          />{" "}
          <span className="customRadio" />
          não usar cupom
        </ListLabel>
      </ListItem>

      {checkout.availableCoupons.map(coupon => (
        <ListItem key={coupon.id}>
          <ListLabel>
            <input
              onChange={e => selectCoupon(e.target.value)}
              type="radio"
              name="coupon"
              value={coupon.id}
              checked={couponId === coupon.id.toString()}
            />
            <span className="customRadio" /> {coupon.title}
          </ListLabel>
          <span className="highlight"> - {coupon.discount}</span>
        </ListItem>
      ))}
    </List>
    <List>
      <ListTitle>Resumo</ListTitle>
      <ListItem>
        valor original
        <span>{product.price}</span>
      </ListItem>

      {couponId && checkout.availableCoupons[0] && (
        <ListItem key={checkout.availableCoupons[0].id}>
          cupom
          <span className="highlight">
            - {checkout.availableCoupons[0].discount}
          </span>
        </ListItem>
      )}

      <ListItem>
        frete
        <span>{checkout.shippingPrice}</span>
      </ListItem>

      <ListItem>
        total <strong>{checkout.totalPrice}</strong>
      </ListItem>
    </List>
  </Container>
);

ProductDetail.propTypes = {
  product: PropTypes.shape({
    image: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.string
  }).isRequired,
  checkout: PropTypes.shape({
    availableCoupons: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        discount: PropTypes.string.isRequired
      })
    ),
    shippingPrice: PropTypes.string,
    totalPrice: PropTypes.string
  }).isRequired,
  couponId: PropTypes.string.isRequired,
  selectCoupon: PropTypes.func.isRequired
};

export default ProductDetail;
