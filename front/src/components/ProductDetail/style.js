import styled from "styled-components";

export const Container = styled.div`
  padding: 0 20px;
`;

export const Img = styled.img`
  border-radius: 4px;
  display: block;
  margin: 30px auto;
`;

export const ListTitle = styled.strong`
  color: #5b5855;
  display: block;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 4px;
`;

export const List = styled.ul`
  list-style: none;
  margin-bottom: 30px;
`;

export const ListItem = styled.li`
  color: #7d7a77;
  font-size: 15px;
  display: flex;
  justify-content: space-between;
  padding-top: 14px;

  .highlight {
    color: #e85657;
  }

  strong {
    color: #5b5855;
  }
`;

export const ListLabel = styled.label`
  cursor: pointer;

  display: flex;
  align-items: center;
  justify-content: center;

  input {
    visibility: hidden;
    position: absolute;
    left: 0;

    + .customRadio {
      content: "";
      border: 1px solid #e4e1df;
      border-radius: 50%;
      height: 20px;
      width: 20px;
      margin-right: 6px;

      display: flex;
      align-items: center;
      justify-content: center;
    }

    &:checked {
      + .customRadio {
        &:before {
          content: "";
          background-color: #f77274;
          border-radius: 50%;
          display: inline-block;
          height: 10px;
          width: 10px;
        }
      }
    }
  }
`;
