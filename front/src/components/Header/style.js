import styled from 'styled-components';

export const Container = styled.div`
  padding: 15px;
  border-bottom: 1px solid #f4f2f0;
  display: flex;
  align-items: center;
  justify-content: center;
`;
