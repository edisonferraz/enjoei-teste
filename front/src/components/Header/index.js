import React from 'react';

import { Container } from './style';
import logo from '../../assets/logo.png';

const Header = () => (
  <Container>
    <img src={logo} alt="Enjoei" width="36" height="36" />
  </Container>
);

export default Header;
