import React, { Fragment } from "react";
import Modal from "../index";
import iconCartCancel from "../../../assets/icon-cart-cancel.png";

export default props => (
  <Modal {...props}>
    <Fragment>
      <img src={iconCartCancel} alt="cart" width="30" />
      <div>
        <strong>compra cancelada</strong>
        <p>
          o pedido não foi enviado <br /> e você não sera cobrado
        </p>
      </div>
    </Fragment>
  </Modal>
);
