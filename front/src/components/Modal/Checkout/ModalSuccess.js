import React, { Fragment } from "react";
import Modal from "../index";
import iconCartSuccess from "../../../assets/icon-cart-success.png";

export default props => (
  <Modal {...props}>
    <Fragment>
      <img src={iconCartSuccess} alt="cart" width="30" />
      <div>
        <strong>compra confirmada</strong>
        <p>
          enviaremos atualizações sobre <br /> o pedido para o seu email
        </p>
      </div>
    </Fragment>
  </Modal>
);
