import React from "react";
import PropTypes from "prop-types";

import { Overlay, Container, Body } from "./style";

const Modal = ({ children, closeModal }) => (
  <Container>
    <Overlay onClick={closeModal} title="Close" />
    <Body>{children}</Body>
  </Container>
);

Modal.defaultProps = {
  children: <div>Modal Content</div>
};

Modal.propTypes = {
  children: PropTypes.element,
  closeModal: PropTypes.func.isRequired
};

export default Modal;
