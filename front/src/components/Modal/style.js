import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

export const Overlay = styled.div`
  background: rgba(0, 0, 0, 0.8);
  cursor: pointer;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

export const Body = styled.div`
  background-color: #fff;
  border-radius: 4px;
  color: #7d7a77;
  font-size: 16px;
  padding: 30px;
  height: 200px;
  text-align: center;
  max-width: 350px;
  width: 80%;
  position: relative;
  z-index: 2;

  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-direction: column;

  strong {
    color: #5b5855;
    display: block;
    font-weight: 600;
    font-size: 18px;
    margin: 30px 0 16px 0;
  }
`;
