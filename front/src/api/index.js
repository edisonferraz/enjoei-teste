import { BASE_URL } from "./constants";
import { formatPrices } from "../utils";

export const getCheckout = async (checkoutId, couponId) => {
  const couponQuery = couponId ? `?couponId=${couponId}` : "";
  const res = await fetch(`${BASE_URL}/checkouts/${checkoutId}${couponQuery}`);
  const data = await res.json();

  return formatPrices(data);
};

export const confirmCheckout = async checkoutId => {
  await fetch(`${BASE_URL}/checkouts/${checkoutId}`, {
    method: "post",
    body: JSON.stringify({ checkoutId }),
    headers: {
      "Content-Type": "application/json"
    }
  });
};
