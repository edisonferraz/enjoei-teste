import React, { Fragment } from 'react';
import GlobalStyle from './styles/global';

import Checkout from './pages/Checkout';

const App = () => (
  <Fragment>
    <GlobalStyle />
    <Checkout />
  </Fragment>
);

export default App;
