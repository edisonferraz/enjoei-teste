import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box
  }

  html, body, #root {
    height: 100%;
  }

  body {
    font-family: 'Montserrat', sans-serif;
  }

  /* #root {
    display: flex;
    align-items: center;
    justify-content: center;
  } */
`;
export default GlobalStyle;
