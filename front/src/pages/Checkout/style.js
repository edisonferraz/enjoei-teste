import styled from "styled-components";

export const Container = styled.div`
  max-width: 500px;

  @media (min-width: 500px) {
    border: 1px solid #eee;
  }
`;

export const ContainerButtons = styled.div`
  border-top: 1px solid #f5f3f1;
  padding: 15px 20px;
  display: flex;
  justify-content: space-between;

  button {
    width: 47%;
  }
`;

export const Button = styled.button`
  background-color: ${props => (props.white ? "#fff" : "#f05b78")};
  border: 1px solid ${props => (props.white ? "#e4e1df" : "#f05b78")};
  border-bottom: 3px solid ${props => (props.white ? "#e4e1df" : "#d25069")};
  border-radius: 4px;
  color: ${props => (props.white ? "#f77274" : "#fff")};
  cursor: pointer;
  font: bold 14px "Montserrat", sans-serif;
  height: 50px;
  padding: 0 30px;
  transition: all ease-in-out 0.1s;

  &:hover {
    background-color: ${props => (props.white ? "#fff" : "#f16984")};
    border: 1px solid ${props => (props.white ? "#d8d4d1" : "#f16984")};
    border-bottom: 3px solid ${props => (props.white ? "#d8d4d1" : "#d25069")};
  }
`;
