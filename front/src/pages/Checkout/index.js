import React, { Component } from "react";
import { setTimeout } from "timers";
import { getCheckout, confirmCheckout } from "../../api";

import { Container, ContainerButtons, Button } from "./style";

import Header from "../../components/Header";
import ProductDetail from "../../components/ProductDetail";
import ModalCancel from "../../components/Modal/Checkout/ModalCancel";
import ModalSucess from "../../components/Modal/Checkout/ModalSuccess";

class Checkout extends Component {
  state = {
    product: {},
    checkout: {
      availableCoupons: []
    },
    checkoutId: "20181112",
    couponId: "",
    showModalCancel: false,
    showModalSuccess: false,
    loading: false
  };

  componentDidMount() {
    const { checkoutId } = this.state;
    getCheckout(checkoutId).then(data =>
      this.setState({ product: data.product, checkout: data.checkout })
    );
  }

  selectCoupon = couponId => {
    const { checkoutId } = this.state;

    getCheckout(checkoutId, couponId).then(data =>
      this.setState({
        product: data.product,
        checkout: data.checkout,
        couponId
      })
    );
  };

  successCheckout = () => {
    const { checkoutId } = this.state;

    this.setState({ loading: true });

    confirmCheckout(checkoutId).then(() =>
      setTimeout(() => {
        this.setState({ showModalSuccess: true, loading: false });
      }, 1000)
    );
  };

  cancelCheckout = () => {
    this.setState({ showModalCancel: true });
  };

  closeModal = () => {
    this.setState({
      showModalCancel: false,
      showModalSuccess: false
    });
  };

  render() {
    const {
      product,
      checkout,
      couponId,
      showModalCancel,
      showModalSuccess,
      loading
    } = this.state;

    return (
      <Container>
        <Header />

        <ProductDetail
          product={product}
          checkout={checkout}
          couponId={couponId}
          selectCoupon={this.selectCoupon}
        />

        <ContainerButtons>
          <Button onClick={this.cancelCheckout} white>
            Cancelar
          </Button>
          <Button onClick={this.successCheckout}>
            {loading ? "Processando..." : "Confirmar"}
          </Button>
        </ContainerButtons>

        {showModalCancel && <ModalCancel closeModal={this.closeModal} />}
        {showModalSuccess && <ModalSucess closeModal={this.closeModal} />}
      </Container>
    );
  }
}

export default Checkout;
