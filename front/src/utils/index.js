const currencyFormat = value =>
  value.toLocaleString("pt-br", {
    style: "currency",
    currency: "BRL"
  });

export const formatPrices = data => ({
  product: {
    ...data.product,
    price: currencyFormat(data.product.price)
  },
  checkout: {
    ...data.checkout,
    shippingPrice: currencyFormat(data.checkout.shippingPrice),
    availableCoupons: [
      {
        ...data.checkout.availableCoupons[0],
        discount: currencyFormat(data.checkout.availableCoupons[0].discount)
      }
    ],
    totalPrice: currencyFormat(data.checkout.totalPrice)
  }
});
