### Instructions

1. Install the dependencies with `npm install`.

2. Run the application with `npm start`.

3. Enjoy!
